require 'minitest/autorun'
require_relative '../range_list'

class TestRangeList < MiniTest::Test
  def setup
    @rl = RangeList.new
  end

  def test_validate_range
    assert_equal :invalid, @rl.validate_range, 'empty params returns invalid'
    assert_equal :invalid, @rl.validate_range([1]), 'params size is not equal 2 returns invalid'
    assert_equal :invalid, @rl.validate_range([1, 2, 3]), 'params size is not equal 2 returns invalid'
    assert_equal :invalid, @rl.validate_range([1, 2, 3]), 'params size is not equal 2 returns invalid'
    assert_equal :valid, @rl.validate_range([1, 2]), 'params size equal 2 returns valid'
    assert_equal :invalid, @rl.validate_range([3, 2]), 'params number sort order is not asceding returns invalid'
  end

  def test_overlaps
    assert_equal false, @rl.overlaps([1, 3], [4, 9]), 'not overlaped'
    assert_equal false, @rl.overlaps([4, 9], [1, 3]), 'not overlaped'
    assert_equal true, @rl.overlaps([1, 3], [2, 9]), 'overlaped'
  end

  def test_add
    @rl.add([1, 5])
    assert_equal [[1, 5]], @rl.ranges, 'inserted'

    @rl.add([1])
    assert_equal [[1, 5]], @rl.ranges, 'not inserted because invalid params'

    @rl.add([5, 1])
    assert_equal [[1, 5]], @rl.ranges, 'not inserted because number descends'

    @rl.add([10, 20])
    assert_equal [[1,5], [10, 20]], @rl.ranges, 'inserted'

    @rl.add([20, 20])
    assert_equal [[1,5], [10, 20]], @rl.ranges, 'not inserted because range not valid'

    @rl.add([20, 21])
    assert_equal [[1,5], [10, 21]], @rl.ranges, 'inserted'

    @rl.add([2, 4])
    assert_equal [[1,5], [10, 21]], @rl.ranges, 'involved so rangelist remains unchanged'

    @rl.add([3, 8])
    assert_equal [[1,8], [10, 21]], @rl.ranges, 'rangelist changed'
  end

  def test_remove
    @rl.add([1, 8])
    @rl.add([10, 21])

    @rl.remove([10, 10])
    assert_equal [[1,8], [10, 21]], @rl.ranges, 'range not removed'

    @rl.remove([10, 11])
    assert_equal [[1,8], [11, 21]], @rl.ranges, 'removed'

    @rl.remove([15, 17])
    assert_equal [[1,8], [11, 15], [17, 21]], @rl.ranges, 'removed and rangelist splited'

    @rl.remove([3, 19])
    assert_equal [[1,3], [19, 21]], @rl.ranges, 'removed and rangelist changed'
  end
end