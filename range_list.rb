class RangeList
	attr_accessor :ranges
	VALID_RANGE_LENGTH = 2

	def initialize(params = [])
		@ranges = params
	end

	def validate_range(range = [])
		if range.size != VALID_RANGE_LENGTH
			return :invalid
		end
			
		start_number_for_range = range.first
		end_number_for_range = range.last
		if start_number_for_range >= end_number_for_range
			return :invalid
		end
		return :valid
	end

	def overlaps(rangeA, rangeB)
		return false if rangeA.last < rangeB.first
		return false if rangeA.first > rangeB.last
		return true
	end

	def add(range)
		return if validate_range(range) == :invalid

		if @ranges.empty?
			@ranges.push range
		else
			start_number_for_range = range.first
			end_number_for_range = range.last
			
			# head and tail part
			ranges_before = @ranges.select{|r| r[1] < start_number_for_range}
			ranges_after = @ranges.select{|r| r[0] > end_number_for_range}

			# middle part
			ranges_overlap = @ranges.select{|r| overlaps(r, range)}
			overlap_start = [start_number_for_range, ranges_overlap.map{|item| item[0]}].flatten.min
			overlap_end = [end_number_for_range, ranges_overlap.map{|item| item[1]}].flatten.max

			# concat together
			@ranges = [].concat(ranges_before, [[overlap_start, overlap_end]], ranges_after)
		end
	end

	def remove(range)
		if validate_range(range) == :invalid
			return
		end

		return if @ranges.empty?

		start_number_for_range = range.first
		end_number_for_range = range.last

		result = []
		index = 0
		@ranges.each do |item|
			left_number = item.first
			right_number = item.last

			# no overlapping case
			if start_number_for_range > right_number || end_number_for_range < left_number
				result.push @ranges[index]
				index += 1
				next
			end

			# new range covers the whole range
			if start_number_for_range <= left_number && right_number <= end_number_for_range
				index += 1
				next
			end

			# overlap happens on the head part
			if start_number_for_range <= left_number && end_number_for_range < right_number
				@ranges[index][0] = end_number_for_range
				result.push(@ranges[index])
				index += 1
				next
			end

			# overlap happens in the middle of item, split one range to two ranges
			if left_number < start_number_for_range && end_number_for_range < right_number
				result.push([left_number, start_number_for_range], [end_number_for_range, right_number])
				index += 1
				next
			end

			# overlap happens on tail part of item
			if left_number < start_number_for_range  && right_number <= end_number_for_range
				@ranges[index][1] = start_number_for_range
				result.push(@ranges[index])
				index += 1
			end
		end
		@ranges = result
	end

	def print
		result = ''
		@ranges.each {|n| result += "[#{n[0]}, #{n[1]}) " }
		p result
	end
end

rl = RangeList.new
rl.add([1, 5])
rl.print

rl.add([10, 20])
rl.print

rl.add([20, 20])
rl.print

rl.add([20, 21])
rl.print

rl.add([2, 4])
rl.print

rl.add([3, 8])
rl.print

rl.remove([10, 10])
rl.print

rl.remove([10, 11])
rl.print

rl.remove([15, 17])
rl.print

rl.remove([3, 19])
rl.print